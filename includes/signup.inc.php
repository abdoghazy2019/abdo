<?php
if(isset($_POST["signup-button"])){
$uname=$_POST["uname"];
$mail=$_POST["mail"];
$pwd=$_POST["pwd"];
$pwdr=$_POST["pwdr"];
	
if(empty($uname) or empty($mail) or empty($pwd) or empty($pwdr)) {
	
header("location:../signup.php?error=emptyfields");
exit(); }
elseif(!preg_match("/^[a-zA-Z0-9]*$/",$uname) or strlen($uname) <6 or strlen($uname) >20){
header("location:../signup.php?error=invalidusername");
exit(); }
elseif(!filter_var($mail, FILTER_VALIDATE_EMAIL)){header("location:../signup.php?error=invalidemail");
exit();}
elseif(strlen($pwd) <8){
header("location:../signup.php?error=weakpassword");
exit();}
elseif($pwd !== $pwdr) {header("location:../signup.php?error=notmatchpassword"); exit();}
else{
require "dbconn.php";
$sql="SELECT user_name , email from users1 WHERE user_name=? OR email=? LIMIT 1;";
$stmt=mysqli_stmt_init($conn);
if(!mysqli_stmt_prepare($stmt,$sql)){header("location:../signup.php?error=s");
exit();}
else{
mysqli_stmt_bind_param($stmt,"ss",$uname,$mail);
mysqli_stmt_execute($stmt);
mysqli_stmt_store_result($stmt);
$result=mysqli_stmt_num_rows($stmt);
if($result >0){
header("location:../signup.php?error=exist");
mysqli_stmt_close($stmt);
mysqli_close($conn);
exit();}
else{
$query="INSERT INTO users1 (user_name,email,password) VALUES (?,?,?)";
$stmt=mysqli_stmt_init($conn);
if(!mysqli_stmt_prepare($stmt,$query)){
	header("location: ../signup.php?error=s2");
	exit();}
else{
$hashpass=password_hash($pwd,PASSWORD_DEFAULT);
mysqli_stmt_bind_param($stmt,"sss",$uname,$mail,$hashpass);
mysqli_stmt_execute($stmt);
header("location:../signup.php?signup=sucsess");
}
}
}
}
mysqli_stmt_close($stmt);
mysqli_close($conn);
exit();
}
else{
	header("location:../index.php?error=404notfound");
	exit();
}



	